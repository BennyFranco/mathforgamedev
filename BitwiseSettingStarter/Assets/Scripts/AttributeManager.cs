﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttributeManager : MonoBehaviour
{
    static public int MAGIC = 16;
    static public int INTELLIGENCE = 8;
    static public int CHARISMA = 4;
    static public int FLY = 2;
    static public int INVISIBLE = 1;

    public Text attributeDisplay;
    public int attributes = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(this.transform.position);
        attributeDisplay.transform.position = screenPoint + new Vector3(0,-50,0);
        attributeDisplay.text = Convert.ToString(attributes,2).PadLeft(8,'0');
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("MAGIC"))
        {
            attributes ^= MAGIC;
        }
        else if(other.gameObject.CompareTag("INTELLIGENCE"))
        {
            attributes ^= INTELLIGENCE;
        }
        else if(other.gameObject.CompareTag("CHARISMA"))
        {
            attributes ^= CHARISMA;
        }
        else if(other.gameObject.CompareTag("FLY"))
        {
            attributes ^= FLY;
        }
        else if(other.gameObject.CompareTag("INVISIBLE"))
        {
            attributes ^= INVISIBLE;
        } 
        else if(other.gameObject.CompareTag("ANTIMAGIC"))
        {
            attributes &= ~MAGIC;
        }
        else if(other.gameObject.CompareTag("POWER"))
        {
            attributes |= (INTELLIGENCE | MAGIC | CHARISMA);
        }
        else if(other.gameObject.CompareTag("LESSPOWER"))
        {
            attributes &= ~(INTELLIGENCE | MAGIC);
        }
          else if(other.gameObject.CompareTag("RESET"))
        {
            attributes = 0 ;
        }
    }
       
}
