﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyManager : MonoBehaviour
{
    static public int RED = 4;
    static public int YELLOW = 2;
    static public int BLUE = 1;
    public Text attributeDisplay;
    public int keys = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 screenPoint = Camera.main.WorldToScreenPoint(this.transform.position);
        attributeDisplay.transform.position = screenPoint + new Vector3(0,-50,0);
        attributeDisplay.text = Convert.ToString(keys,2).PadLeft(8,'0');
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.CompareTag("RED"))
        {
            keys |= RED;
        }
        else if(other.gameObject.CompareTag("YELLOW"))
        {
            keys |= YELLOW;
        }
        else if(other.gameObject.CompareTag("BLUE"))
        {
            keys |= BLUE;
        }
        else if(other.gameObject.CompareTag("MASTER"))
        {
            keys |= (RED | YELLOW | BLUE);
        }
        else if(other.gameObject.CompareTag("RESET"))
        {
            keys = 0 ;
        }

        if(!other.GetComponent<MazeDoorManager>())
            Destroy(other.gameObject);
    }
       
}
