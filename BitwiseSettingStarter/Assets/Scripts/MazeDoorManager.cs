﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MazeDoorManager : MonoBehaviour
{
    [SerializeField]
     int doorType = KeyManager.RED;

    void OnCollisionEnter(Collision collision)
    {
        if((collision.gameObject.GetComponent<KeyManager>().keys & doorType) != 0)
        {
            GetComponent<BoxCollider>().isTrigger = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
         GetComponent<BoxCollider>().isTrigger = false;
         other.gameObject.GetComponent<KeyManager>().keys &= ~doorType;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
