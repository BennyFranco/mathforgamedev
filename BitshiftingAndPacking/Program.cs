﻿using System;

namespace BitshiftingAndPacking
{
    class Program
    {
        static void FirstExercise()
        {
            string A = "110111";
            string B = "10001";
            string C = "1101";

            int aBits = Convert.ToInt32(A,2);
            int bBits = Convert.ToInt32(B,2);
            int cBits = Convert.ToInt32(C,2);

            int packed = 0;

            packed = packed | (aBits << 26);
            packed = packed | (bBits << 21);
            packed = packed | (cBits << 17);

            Console.WriteLine(Convert.ToString(packed,2).PadLeft(32,'0'));
        }

        static void SecondExercise()
        {
            string A = "1111";
            string B = "101";
            string C = "11011";

            int aBits = Convert.ToInt32(A,2);
            int bBits = Convert.ToInt32(B,2);
            int cBits = Convert.ToInt32(C,2);

            int packed = 0;

            packed = packed | (aBits << 28);
            packed = packed | (bBits << 25);
            packed = packed | (cBits << 20);

            Console.WriteLine(Convert.ToString(packed,2).PadLeft(32,'0'));
        }

        static void Quiz()
        {
            // int A = 48 >> 4;

            // Console.WriteLine(Convert.ToString(A,2).PadLeft(32,'0'));

            // string A = "10101010";
            // int aBits = Convert.ToInt32(A,2);
            // aBits = aBits >> 2;

            //  Console.WriteLine(Convert.ToString(aBits,10));

            // int A = 3488;
            // Console.WriteLine(Convert.ToString(A,2).PadLeft(32,'0'));

            // string A = "110";
            // string B = "1010";
            // string C = "11";

            // Int64 aBits = Convert.ToInt64(A,2);
            // Int64 bBits = Convert.ToInt64(B,2);
            // Int64 cBits = Convert.ToInt64(C,2);

            // Int64 packed = 0;

            // packed = packed | (aBits << 56);
            // packed = packed | (bBits << 48);
            // packed = packed | (cBits << 40);

            // Console.WriteLine(Convert.ToString(packed,2).PadLeft(64,'0'));

            int A = 3;
            int B = 3;
            int C = 2;

            Console.WriteLine(Convert.ToString(A,2).PadLeft(8,'0'));
            Console.WriteLine(Convert.ToString(B,2).PadLeft(8,'0'));
            Console.WriteLine(Convert.ToString(C,2).PadLeft(8,'0'));

            // int packed = 0;

            // packed = packed | (A << 56);
            // packed = packed | (B << 48);
            // packed = packed | (C << 40);

            // Console.WriteLine(Convert.ToString(packed,2).PadLeft(32,'0'));
        }

        static void Main(string[] args)
        {
            // FirstExercise();
            // SecondExercise();
            Quiz();
        }

       
    }
}
