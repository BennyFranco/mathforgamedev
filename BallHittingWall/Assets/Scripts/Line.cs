﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Line
{
    public Coords A;
    public Coords B;
    Coords v;

    public enum LINETYPE { LINE, SEGMENT, RAY };
    LINETYPE type;

    public Line(Coords _A, Coords _B, LINETYPE _type)
    {
        A = _A;
        B = _B;
        type = _type;
        v = new Coords(B - A);
    }

    public Line(Coords _A, Coords _v)
    {
        A = _A;
        B = _A + _v;
        v = _v;
        type = LINETYPE.SEGMENT;
    }

    public float IntersectAt(Line l)
    {
        if(ErgoMath.Dot(l.v.Perp(), v) == 0)
        {
            return float.NaN;
        }

        Coords c = l.A - A;
        float t = ErgoMath.Dot(l.v.Perp(), c) / ErgoMath.Dot(l.v.Perp(), v);

        if( (t < 0 || t > 1) && type == LINETYPE.SEGMENT)
        {
            return float.NaN;
        }
        
        return t;
    }

    public Coords Lerp(float t)
    {
        if (type == LINETYPE.SEGMENT)
            t = Mathf.Clamp(t, 0, 1);
        else if (type == LINETYPE.RAY && t < 0)
            t = 0;

        return A + v * t;
    }

    public void Draw(float width, Color color)
    {
        Coords.DrawLine(A, B, width, color);
    }
}
