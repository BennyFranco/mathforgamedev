﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plane
{
    Coords A;
    Coords B;
    Coords C;
    Coords v;
    Coords u;

    public Plane(Coords _A, Coords _B, Coords _C)
    {
        A = _A;
        B = _B;
        C = _C;
        v = B - A;
        u = C - A;
    }

    public Plane(Coords _A, Vector3 V, Vector3 U)
    {
        A = _A;
        v = new Coords(V);
        u = new Coords(U);
    }

    public Coords Lerp(float s, float t)
    {
        return A + v * s + u * t;
    }
}
