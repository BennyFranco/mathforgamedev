﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawLines : MonoBehaviour
{
    Coords point = new Coords(10, 20);

    Coords startPointX = new Coords(160, 0);
    Coords endPointX = new Coords(-160, 0);
    Coords startPointY = new Coords(0, 100);
    Coords endPointY = new Coords(0, -100);

    // Start is called before the first frame update
    void Start()
    {
        // Debug.Log(point.ToString());
        // Coords.DrawPoint(new Coords(0,0), 2, Color.red);
        // Coords.DrawPoint(point, 2, Color.green);

        // Coords.DrawLine(startPointX, endPointX, 1, Color.red);
        // Coords.DrawLine(startPointY, endPointY, 1, Color.green);

        List<Coords> coords = new List<Coords>();
        coords.Add(new Coords(25, 45));
        coords.Add(new Coords(10, 30));
        coords.Add(new Coords(5, 25));
        coords.Add(new Coords(15, 2));
        coords.Add(new Coords(45, -1));
        coords.Add(new Coords(55, 3));
        coords.Add(new Coords(60, 33));
        coords.Add(new Coords(63, 52));
        coords.Add(new Coords(83, 82));
        coords.Add(new Coords(93, 89));

        Coords.DrawLine(coords[0], coords[1], 0.5f, Color.white, Color.yellow);

        for(int i = 0; i < coords.Count-1; i++)
        {
            Coords.DrawLine(coords[i], coords[i+1], 0.5f, Color.white);
        }
        
        Coords.DrawLine(coords[coords.Count-1], new Coords(113, 120), 0.5f, Color.white, Color.yellow);
        Coords.DrawLine(coords[coords.Count-1], new Coords(120, 100), 0.5f, Color.white, Color.yellow);
        Coords.DrawLine(coords[coords.Count-1], new Coords(120, 85), 0.5f, Color.white, Color.yellow);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
