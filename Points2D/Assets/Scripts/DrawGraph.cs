﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawGraph : MonoBehaviour
{
    public int Size = 20;
    public int Xmax = 200;
    public int Ymax = 200;

    // Start is called before the first frame update
    void Start()
    {
        DrawGraphs();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void DrawGraphs()
    {
        Coords start = new Coords(Xmax, 0);
        Coords end = new Coords(-Xmax, 0);

        Coords.DrawLine(start, end, 0.5f, Color.red);

        start = new Coords(0, Ymax);
        end = new Coords(0, -Ymax);

        Coords.DrawLine(start, end, 0.5f, Color.green);

        DrawVerticalLines();
        DrawHorizontalLines();
    }

    void DrawVerticalLines()
    {
        int xoffset = (int)(Xmax / (float)Size);

        for (int x = -xoffset * Size; x <= xoffset * Size; x += Size)
        {
            Coords start = new Coords(x, Ymax);
            Coords end = new Coords(x, -Ymax);

            Coords.DrawLine(start, end, 0.5f, Color.white);
        }
    }

    void DrawHorizontalLines()
    {
        int yoffset = (int)(Ymax / (float)Size);

        for (int y = -yoffset * Size; y <= yoffset * Size; y += Size)
        {
            Coords start = new Coords(-Xmax, y);
            Coords end = new Coords(Xmax, y);

            Coords.DrawLine(start, end, 0.5f, Color.white);
        }
    }
}
