﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxisRotate : MonoBehaviour
{
    public GameObject[] points;
    public Vector3 angle;

    // Start is called before the first frame update
    void Start()
    {
        angle = angle * Mathf.Deg2Rad;
        foreach (GameObject point in points)
        {
            Coords position = new Coords(point.transform.position, 1);
            point.transform.position = ErgoMath.Rotate(position, angle.x, true, angle.y, true, angle.z, true).ToVector();
        }

        Matrix rot = ErgoMath.GetRotationMatrix(angle.x, true, angle.y, true, angle.z, true);
        float rotAngle = ErgoMath.GetRoationAxisAngle(rot);
        Coords rotAxis = ErgoMath.GetRotationAxis(rot, rotAngle);

        Debug.Log(rotAngle * Mathf.Rad2Deg + " about " + rotAxis.ToString());
        Coords.DrawLine(new Coords(0, 0, 0), rotAxis * 5, 0.1f, Color.yellow);
    }
}
