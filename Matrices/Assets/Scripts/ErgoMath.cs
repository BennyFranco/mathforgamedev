﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErgoMath
{
    static public Coords GetNormal(Coords vector)
    {
        float length = Distance(new Coords(0, 0), vector);

        vector.x /= length;
        vector.y /= length;
        vector.z /= length;

        return vector;
    }

    static public Coords GetNormal(Coords vector1, Coords vector2)
    {
        vector2 = DistanceCoords(vector1, vector2);

        return GetNormal(vector2);
    }

    static public Coords DistanceCoords(Coords point1, Coords point2)
    {
        Coords result = new Coords(point2.x - point1.x, point2.y - point1.y, point2.z - point1.z);

        return result;
    }

    static public float Distance(Coords point1, Coords point2)
    {
        float diffSquared = Square(point1.x - point2.x)
                            + Square(point1.y - point2.y)
                            + Square(point1.z - point2.z);

        float squareRoot = SquareRoot(diffSquared);

        return squareRoot;
    }

    static public Coords Lerp(Coords A, Coords B, float t)
    {
        t = Mathf.Clamp(t, 0, 1);
        Coords v = B - A;
        v = A + v * t;

        return v;
    }

    static public float Square(float value)
    {
        return value * value;
    }

    static public float SquareRoot(float value)
    {
        float r = value;
        float t = 0.0f;

        int i = 0;

        while (t != r || i == 10)
        {
            t = r;
            r = (1.0f / 2.0f) * ((value / r) + r);

            i++;
        }

        return r;
    }

    static public float Dot(Coords vector1, Coords vector2)
    {
        return ((vector1.x * vector2.x) + (vector1.y * vector2.y) + (vector1.z * vector2.z));
    }

    static public float Angle(Coords vector1, Coords vector2)
    {
        float dot = Dot(vector1, vector2);

        Coords origin = new Coords(0, 0, 0);
        float distOfV1 = Distance(origin, vector1);
        float distOfV2 = Distance(origin, vector2);

        float theta = dot / (distOfV1 * distOfV2);

        float angle = Mathf.Acos(theta); // Radians. For degrees multiply angle*180/pi

        return angle;
    }

    static public float RadToDeg(float radians)
    {
        return radians * (180.0f / Mathf.PI);
    }

    static public float DegToRad(float degrees)
    {
        return degrees * (Mathf.PI / 180.0f);
    }

    static public Coords Translate(Coords position, Coords facing, Coords vector)
    {
        if (Distance(new Coords(0, 0, 0), vector) <= 0)
        {
            return position;
        }

        float angle = Angle(vector, facing);
        float worldAngle = Angle(vector, new Coords(0, 1, 0));
        bool clockwise = false;
        if (Cross(vector, facing).z < 0)
            clockwise = true;
        vector = Rotate(vector, angle + worldAngle, clockwise);
        return position + vector;
    }

    static public Coords Translate(Coords position, Coords vector)
    {
        Matrix qMatrix = vector.AsTranslationMatrix() * position.AsMatrix();
        return qMatrix.AsCoords();
    }

    static public Coords Scale(Coords position, float scaleX, float scaleY, float scaleZ)
    {
        float[] values = { scaleX, 0, 0, 0, 0, scaleY, 0, 0, 0, 0, scaleZ, 0, 0, 0, 0, 1 };
        Matrix scaleMatrix = new Matrix(4, 4, values);

        return (scaleMatrix * position.AsMatrix()).AsCoords();
    }

    static public Coords Rotate(Coords position, float angleX, bool clockwiseX,
                                                 float angleY, bool clockwiseY,
                                                 float angleZ, bool clockwiseZ)
    {
        if (clockwiseX)
            angleX = 2 * Mathf.PI - angleX;
        if (clockwiseY)
            angleY = 2 * Mathf.PI - angleY;
        if (clockwiseZ)
            angleZ = 2 * Mathf.PI - angleZ;

        float[] xValues =
        {
            1,0,0,0,
            0, Mathf.Cos(angleX), -Mathf.Sin(angleX), 0,
            0, Mathf.Sin(angleX),  Mathf.Cos(angleX), 0,
            0,0,0,1
        };

        float[] yValues =
        {
            Mathf.Cos(angleY), 0 , Mathf.Sin(angleY), 0,
            0, 1, 0, 0,
            -Mathf.Sin(angleY), 0, Mathf.Cos(angleY), 0,
            0,0,0,1
        };

        float[] zValues =
       {
            Mathf.Cos(angleZ), -Mathf.Sin(angleZ), 0, 0,
            Mathf.Sin(angleZ),  Mathf.Cos(angleZ), 0, 0,
            0,0,1,0,
            0,0,0,1
        };

        Matrix XRoll = new Matrix(4, 4, xValues);
        Matrix YRoll = new Matrix(4, 4, yValues);
        Matrix ZRoll = new Matrix(4, 4, zValues);

        return (ZRoll * YRoll * XRoll * position.AsMatrix()).AsCoords();
    }

    static public Matrix GetRotationMatrix(float angleX, bool clockwiseX,
                                           float angleY, bool clockwiseY,
                                           float angleZ, bool clockwiseZ)
    {
        if (clockwiseX)
            angleX = 2 * Mathf.PI - angleX;
        if (clockwiseY)
            angleY = 2 * Mathf.PI - angleY;
        if (clockwiseZ)
            angleZ = 2 * Mathf.PI - angleZ;

        float[] xValues =
        {
            1,0,0,0,
            0, Mathf.Cos(angleX), -Mathf.Sin(angleX), 0,
            0, Mathf.Sin(angleX),  Mathf.Cos(angleX), 0,
            0,0,0,1
        };

        float[] yValues =
        {
            Mathf.Cos(angleY), 0 , Mathf.Sin(angleY), 0,
            0, 1, 0, 0,
            -Mathf.Sin(angleY), 0, Mathf.Cos(angleY), 0,
            0,0,0,1
        };

        float[] zValues =
       {
            Mathf.Cos(angleZ), -Mathf.Sin(angleZ), 0, 0,
            Mathf.Sin(angleZ),  Mathf.Cos(angleZ), 0, 0,
            0,0,1,0,
            0,0,0,1
        };

        Matrix XRoll = new Matrix(4, 4, xValues);
        Matrix YRoll = new Matrix(4, 4, yValues);
        Matrix ZRoll = new Matrix(4, 4, zValues);

        return ZRoll * YRoll * XRoll;
    }

    static public float GetRoationAxisAngle(Matrix rotation)
    {
        return Mathf.Acos(0.5f * (rotation.GetValue(0, 0)
                                    + rotation.GetValue(1, 1)
                                    + rotation.GetValue(2, 2)
                                    + rotation.GetValue(3, 3) - 2));
    }

    static public Coords GetRotationAxis(Matrix rotation, float angle)
    {
        float vx = (rotation.GetValue(2, 1) - rotation.GetValue(1, 2)) / (2 * Mathf.Sin(angle));
        float vy = (rotation.GetValue(0, 2) - rotation.GetValue(2, 0)) / (2 * Mathf.Sin(angle));
        float vz = (rotation.GetValue(1, 0) - rotation.GetValue(0, 1)) / (2 * Mathf.Sin(angle));

        return new Coords(vx, vy, vz, 0);
    }

    static public Coords Shear(Coords position, float shearX, float shearY, float shearZ)
    {
        float[] values =
        {
            1, shearY, shearZ, 0,
            shearX, 1, shearZ, 0,
            shearX, shearY, 1, 0,
            0, 0, 0, 1
        };

        Matrix shearMatrix = new Matrix(4, 4, values);

        return (shearMatrix * position.AsMatrix()).AsCoords();
    }

    static public Coords Reflect(Coords position)
    {
        float[] values =
        {
            -1,0,0,0,
            0,1,0,0,
            0,0,1,0,
            0,0,0,1
        };

        Matrix reflectMatrix = new Matrix(4, 4, values);
        return (reflectMatrix * position.AsMatrix()).AsCoords();
    }

    static public Coords QRotate(Coords position, Coords axis, float angle)
    {
        Coords aNormalized = axis.GetNormal();
        float w = Mathf.Cos(angle * Mathf.Deg2Rad / 2);
        float s = Mathf.Sin(angle * Mathf.Deg2Rad / 2);

        Coords q = new Coords(aNormalized.x * s, aNormalized.y * s, aNormalized.z * s, w);

        float[] quaternionValues =
        {
            1-2*q.y*q.y - 2*q.z*q.z,    2*q.x*q.y - 2 * q.w*q.z,    2*q.x*q.z + 2*q.w*q.y,  0,
            2*q.x*q.y+2*q.w*q.z,        1-2*q.x*q.x -2*q.z*q.z,     2*q.y*q.z - 2*q.w*q.x,  0,
            2*q.x*q.z - 2*q.w*q.y,      2*q.y*q.z + 2*q.w*q.x,      1-2*q.x*q.x-2*q.y*q.y,  0,
            0,                          0,                          0,                      1
        };

        Matrix quaternionMatrix = new Matrix(4, 4, quaternionValues);
        return (quaternionMatrix * position.AsMatrix()).AsCoords();
    }

    static public Coords Rotate(Coords vector, float angle, bool clockwise) // angle in radians
    {
        if (clockwise)
            angle = 2 * Mathf.PI - angle;

        float xVal = vector.x * Mathf.Cos(angle) - vector.y * Mathf.Sin(angle);
        float yVal = vector.x * Mathf.Sin(angle) + vector.y * Mathf.Cos(angle);

        return new Coords(xVal, yVal, 0);
    }

    static public Coords Cross(Coords vector1, Coords vector2)
    {
        float x = (vector1.y * vector2.z) - (vector1.z * vector2.y);
        float y = (vector1.z * vector2.x) - (vector1.x * vector2.z);
        float z = (vector1.x * vector2.y) - (vector1.y * vector2.x);

        return new Coords(x, y, z);
    }

    static public Coords LookAt2D(Coords forwardVector, Coords position, Coords focusPoint)
    {
        Coords direction = focusPoint - position;
        float angle = Angle(forwardVector, direction);

        bool clockwise = false;
        if (Cross(forwardVector, direction).z < 0)
            clockwise = true;

        Coords newDirection = Rotate(forwardVector, angle, clockwise);

        return newDirection;
    }

}
