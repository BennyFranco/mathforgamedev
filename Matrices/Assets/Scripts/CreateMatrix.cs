﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateMatrix : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        float[] mVals = { 1, 2, 3, 4, 5, 6 };
        float[] nVals = { 1, 2, 3, 4, 5, 6 };
        Matrix m = new Matrix(2, 3, mVals);
        Matrix n = new Matrix(3, 2, nVals);

        Matrix r = m * n;
        Debug.Log(r.ToString());
    }

    // Update is called once per frame
    void Update()
    {

    }
}
