﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Transformations : MonoBehaviour
{
    public GameObject[] points;
    public Vector3 angles;
    public Vector3 translation;
    public Vector3 scaling;
    public Vector3 shear;
    public GameObject centre;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 centrePoint = new Vector3(centre.transform.position.x, centre.transform.position.y, centre.transform.position.z);

        angles = angles * Mathf.Deg2Rad;

        foreach (GameObject point in points)
        {
            Coords position = new Coords(point.transform.position, 1);
            point.transform.position = ErgoMath.Reflect(position).ToVector();


            // position = ErgoMath.Translate(position, new Coords(-centrePoint, 0));
            // position = ErgoMath.Shear(position, shear.x, shear.y, shear.z);
            // point.transform.position = ErgoMath.Translate(position, new Coords(centrePoint, 0)).ToVector();

            // position = ErgoMath.Translate(position, new Coords(-centrePoint, 0));
            // position = ErgoMath.Rotate(position, angles.x, true, angles.y, true, angles.z, true);
            // point.transform.position = ErgoMath.Translate(position, new Coords(centrePoint, 0)).ToVector();

            // position = ErgoMath.Translate(position, new Coords(-centrePoint, 0));
            // position = ErgoMath.Scale(position, scaling.x, scaling.y, scaling.z);
            // point.transform.position = ErgoMath.Translate(position, new Coords(centrePoint, 0)).ToVector();
        }

        DrawLines(0.05f);
    }

    void DrawLines(float width)
    {
        Coords.DrawLine(new Coords(points[0].transform.position), new Coords(points[1].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[0].transform.position), new Coords(points[6].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[1].transform.position), new Coords(points[3].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[3].transform.position), new Coords(points[5].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[5].transform.position), new Coords(points[7].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[6].transform.position), new Coords(points[7].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[7].transform.position), new Coords(points[1].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[4].transform.position), new Coords(points[2].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[4].transform.position), new Coords(points[5].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[4].transform.position), new Coords(points[6].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[2].transform.position), new Coords(points[0].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[2].transform.position), new Coords(points[3].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[4].transform.position), new Coords(points[9].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[5].transform.position), new Coords(points[9].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[2].transform.position), new Coords(points[8].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[3].transform.position), new Coords(points[8].transform.position), width, Color.yellow);
        Coords.DrawLine(new Coords(points[9].transform.position), new Coords(points[8].transform.position), width, Color.yellow);
    }
}
