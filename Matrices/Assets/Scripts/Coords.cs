﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coords
{
    public float x;
    public float y;
    public float z;
    public float w;

    public Coords(float _V)
    {
        x = _V;
        y = _V;
        z = _V;
        w = 0;
    }

    public Coords(float _X, float _Y)
    {
        x = _X;
        y = _Y;
        z = 0;
        w = 0;
    }

    public Coords(float _X, float _Y, float _Z)
    {
        x = _X;
        y = _Y;
        z = _Z;
        w = 0;
    }

    public Coords(float _X, float _Y, float _Z, float _W)
    {
        x = _X;
        y = _Y;
        z = _Z;
        w = _W;
    }

    public Coords(Vector3 vecpos)
    {
        x = vecpos.x;
        y = vecpos.y;
        z = vecpos.z;
    }

    public Coords(Vector3 vecpos, float _W)
    {
        x = vecpos.x;
        y = vecpos.y;
        z = vecpos.z;
        w = _W;
    }

    public Coords(Coords coords)
    {
        x = coords.x;
        y = coords.y;
        z = coords.z;
    }

    public override string ToString()
    {
        return "(" + x + "," + y + "," + z + ")";
    }

    public Vector3 ToVector()
    {
        return new Vector3(x, y, z);
    }

    public float[] AsFloats()
    {
        float[] values = { x, y, z, w };
        return values;
    }

    public Matrix AsMatrix()
    {
        float[] values = { x, y, z, w };
        return new Matrix(4, 1, values);
    }

    public Matrix AsTranslationMatrix()
    {
        float[] values = { 1, 0, 0, x, 0, 1, 0, y, 0, 0, 1, z, 0, 0, 0, 1 };
        return new Matrix(4, 4, values);
    }

    public Coords Perp()
    {
        return new Coords(-y, x);
    }

    public Coords GetNormal()
    {
        float magnitude = ErgoMath.Distance(new Coords(0, 0, 0), new Coords(x, y, z));
        return new Coords(x / magnitude, y / magnitude, z / magnitude);
    }

    public static void DrawPoint(Coords position, float width, Color colour)
    {
        GameObject line = new GameObject("Point_" + position.ToString());
        LineRenderer lineRenderer = line.AddComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Unlit/Color"));
        lineRenderer.material.color = colour;
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, new Vector3(position.x - width / 3.0f, position.y - width / 3.0f, position.z));
        lineRenderer.SetPosition(1, new Vector3(position.x + width / 3.0f, position.y + width / 3.0f, position.z));
        lineRenderer.startWidth = width;
        lineRenderer.endWidth = width;
    }

    public static void DrawLine(Coords startPoint, Coords endPoint, float width, Color colour)
    {
        GameObject line = new GameObject("Line_" + startPoint.ToString() + " - " + endPoint.ToString());
        LineRenderer lineRenderer = line.AddComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Unlit/Color"));
        lineRenderer.material.color = colour;
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, new Vector3(startPoint.x, startPoint.y, startPoint.z));
        lineRenderer.SetPosition(1, new Vector3(endPoint.x, endPoint.y, endPoint.z));
        lineRenderer.startWidth = width;
        lineRenderer.endWidth = width;
    }

    public static void DrawLine(Coords startPoint, Coords endPoint, float width, Color colour, Color pointColour)
    {
        GameObject line = new GameObject("Line_" + startPoint.ToString() + " - " + endPoint.ToString());
        LineRenderer lineRenderer = line.AddComponent<LineRenderer>();
        lineRenderer.material = new Material(Shader.Find("Unlit/Color"));
        lineRenderer.material.color = colour;
        lineRenderer.positionCount = 2;
        lineRenderer.SetPosition(0, new Vector3(startPoint.x, startPoint.y, startPoint.z));
        lineRenderer.SetPosition(1, new Vector3(endPoint.x, endPoint.y, endPoint.z));
        lineRenderer.startWidth = width;
        lineRenderer.endWidth = width;

        DrawPoint(startPoint, width * 4, pointColour);
        DrawPoint(endPoint, width * 4, pointColour);
    }

    public static Coords operator -(Coords v1, Coords v2)
    {
        return new Coords(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
    }

    public static Coords operator +(Coords v1, Coords v2)
    {
        return new Coords(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
    }

    public static Coords operator *(Coords v, float f)
    {
        return new Coords(v.x * f, v.y * f, v.z * f);
    }

    public static Coords operator *(float f, Coords v)
    {
        return v * f;
    }

    public static Coords operator *(Coords v1, Coords v2)
    {
        return ErgoMath.Cross(v1, v2);
    }

    public static Coords operator /(Coords v, float f)
    {
        return new Coords(v.x / f, v.y / f, v.z / f);
    }
}
