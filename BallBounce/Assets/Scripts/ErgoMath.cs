﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ErgoMath
{
    static public Coords GetNormal(Coords vector)
    {
        float length = Distance(new Coords(0, 0), vector);

        vector.x /= length;
        vector.y /= length;
        vector.z /= length;

        return vector;
    }

    static public Coords GetNormal(Coords vector1, Coords vector2)
    {
        vector2 = DistanceCoords(vector1, vector2);

        return GetNormal(vector2);
    }

    static public Coords DistanceCoords(Coords point1, Coords point2)
    {
        Coords result = new Coords(point2.x - point1.x, point2.y - point1.y, point2.z - point1.z);

        return result;
    }

    static public float Distance(Coords point1, Coords point2)
    {
        float diffSquared = Square(point1.x - point2.x)
                            + Square(point1.y - point2.y)
                            + Square(point1.z - point2.z);

        float squareRoot = SquareRoot(diffSquared);

        return squareRoot;
    }

    static public Coords Lerp(Coords A, Coords B, float t)
    {
        t = Mathf.Clamp(t, 0, 1);
        Coords v = B - A;
        v = A + v * t;

        return v;
    }

    static public float Square(float value)
    {
        return value * value;
    }

    static public float SquareRoot(float value)
    {
        float r = value;
        float t = 0.0f;

        int i = 0;

        while (t != r || i == 10)
        {
            t = r;
            r = (1.0f / 2.0f) * ((value / r) + r);

            i++;
        }

        return r;
    }

    static public float Dot(Coords vector1, Coords vector2)
    {
        return ((vector1.x * vector2.x) + (vector1.y * vector2.y) + (vector1.z * vector2.z));
    }

    static public float Angle(Coords vector1, Coords vector2)
    {
        float dot = Dot(vector1, vector2);

        Coords origin = new Coords(0, 0, 0);
        float distOfV1 = Distance(origin, vector1);
        float distOfV2 = Distance(origin, vector2);

        float theta = dot / (distOfV1 * distOfV2);

        float angle = Mathf.Acos(theta); // Radians. For degrees multiply angle*180/pi

        return angle;
    }

    static public float RadToDeg(float radians)
    {
        return radians * (180.0f / Mathf.PI);
    }

    static public float DegToRad(float degrees)
    {
        return degrees * (Mathf.PI / 180.0f);
    }

    static public Coords Translate(Coords position, Coords facing, Coords vector)
    {
        if (Distance(new Coords(0, 0, 0), vector) <= 0)
        {
            return position;
        }

        float angle = Angle(vector, facing);
        float worldAngle = Angle(vector, new Coords(0, 1, 0));
        bool clockwise = false;
        if (Cross(vector, facing).z < 0)
            clockwise = true;
        vector = Rotate(vector, angle + worldAngle, clockwise);
        return position + vector;
    }

    static public Coords Rotate(Coords vector, float angle, bool clockwise) // angle in radians
    {
        if (clockwise)
            angle = 2 * Mathf.PI - angle;

        float xVal = vector.x * Mathf.Cos(angle) - vector.y * Mathf.Sin(angle);
        float yVal = vector.x * Mathf.Sin(angle) + vector.y * Mathf.Cos(angle);

        return new Coords(xVal, yVal, 0);
    }

    static public Coords Cross(Coords vector1, Coords vector2)
    {
        float x = (vector1.y * vector2.z) - (vector1.z * vector2.y);
        float y = (vector1.z * vector2.x) - (vector1.x * vector2.z);
        float z = (vector1.x * vector2.y) - (vector1.y * vector2.x);

        return new Coords(x, y, z);
    }

    static public Coords LookAt2D(Coords forwardVector, Coords position, Coords focusPoint)
    {
        Coords direction = focusPoint - position;
        float angle = Angle(forwardVector, direction);

        bool clockwise = false;
        if (Cross(forwardVector, direction).z < 0)
            clockwise = true;

        Coords newDirection = Rotate(forwardVector, angle, clockwise);

        return newDirection;
    }

}
