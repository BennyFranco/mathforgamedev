﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateE : MonoBehaviour
{
    public Vector3 eulerAngles;

    Matrix rotationMatrix;
    float angle;
    Coords axis;

    void Start()
    {
        rotationMatrix = ErgoMath.GetRotationMatrix(eulerAngles.x * Mathf.Deg2Rad, false,
                                                    eulerAngles.y * Mathf.Deg2Rad, false,
                                                    eulerAngles.z * Mathf.Deg2Rad, false);
        angle = ErgoMath.GetRoationAxisAngle(rotationMatrix);
        axis = ErgoMath.GetRotationAxis(rotationMatrix, angle);
    }

    void Update()
    {
        // this.transform.forward = ErgoMath.Rotate(new Coords(this.transform.forward, 0),
        //                                             1 * Mathf.Deg2Rad, false,
        //                                             1 * Mathf.Deg2Rad, false,
        //                                             1 * Mathf.Deg2Rad, false).ToVector();

        Coords quaternion = ErgoMath.Quaternion(axis, angle);
        this.transform.rotation *= new Quaternion(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
    }
}
