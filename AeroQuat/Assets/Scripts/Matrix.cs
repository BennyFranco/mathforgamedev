﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class Matrix
{
    float[] values;
    int rows;
    int cols;

    public Matrix(int r, int c, float[] v)
    {
        rows = r;
        cols = c;
        values = new float[rows * cols];
        Array.Copy(v, values, rows * cols);
    }

    public Coords AsCoords()
    {
        if (rows == 4 && cols == 1)
            return new Coords(values[0], values[1], values[2], values[3]);
        else
            return null;
    }

    public float GetValue(int r, int c)
    {
        return values[r * cols + c];
    }

    public override string ToString()
    {
        string matrix = "";

        for (int r = 0; r < rows; r++)
        {
            for (int c = 0; c < cols; c++)
            {
                matrix += values[r * cols + c] + " ";
            }
            matrix += "\n";
        }

        return matrix;
    }

    static public Matrix operator +(Matrix a, Matrix b)
    {
        Assert.AreEqual(a.rows, b.rows, "Rows must be equals in both matrices");
        Assert.AreEqual(a.cols, b.cols, "Cols must be equals in both matrices");

        Matrix result = new Matrix(a.rows, a.cols, a.values);

        int length = a.rows * a.cols;
        for (int i = 0; i < length; i++)
        {
            result.values[i] += b.values[i];
        }

        return result;
    }

    static public Matrix operator *(Matrix a, Matrix b)
    {
        Assert.AreEqual(a.cols, b.rows, "Columns of matrix A must be equals to rows in matrix B");

        float[] resultValues = new float[a.rows * b.cols];

        for (int i = 0; i < a.rows; i++)
        {
            for (int j = 0; j < b.cols; j++)
            {
                for (int k = 0; k < a.cols; k++)
                {
                    resultValues[i * b.cols + j] += a.values[i * a.cols + k] * b.values[k * b.cols + j];
                }
            }
        }

        return new Matrix(a.rows, b.cols, resultValues);
    }
}
