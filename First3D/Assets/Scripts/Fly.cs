﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fly : MonoBehaviour
{
    public float rotationSpeed = 1.0f;
    public float speed = 1.0f;

    // Update is called once per frame
    void Update()
    {
        float rotationX = Input.GetAxis("Vertical") * rotationSpeed;
        float rotationY = Input.GetAxis("Horizontal") * rotationSpeed;
        float rotationZ = Input.GetAxis("HorizontalZ") * rotationSpeed;
        float translateZ = Input.GetAxis("VerticalY") * speed;

        transform.Translate(0, 0, translateZ);
        transform.Rotate(rotationX,rotationY,rotationZ);
    }
}
