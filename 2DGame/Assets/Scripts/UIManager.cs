﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class UIManager : MonoBehaviour
{
    public GameObject tank;
    public GameObject fuel;
    public Text tankPosition;
    public Text fuelPosition;
    public Text energyAmount;
    public InputField angleText;

    public void AddEnergy(string amount)
    {
        int n;
        if (int.TryParse(amount, out n))
        {
            energyAmount.text = amount;
        }
    }

    public void SetAngle(string amount)
    {
        float angle;
        if (float.TryParse(amount, out angle))
        {
            angle *= (Mathf.PI / 180);
            Coords newDirection = ErgoMath.Rotate(new Coords(tank.transform.up), angle, false);
            tank.transform.up = newDirection.ToVector();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        Coords tankCoords = new Coords(tank.transform.up);
        Coords fuelCoords = new Coords(fuel.GetComponent<ObjectManager>().objPosition);

        tankPosition.text = tankCoords + "";
        fuelPosition.text = fuelCoords + "";

        float angle = ErgoMath.Angle(tankCoords, fuelCoords);

        angleText.text = Convert.ToString(ErgoMath.RadToDeg(angle));
        Coords newDirection = ErgoMath.Rotate(tankCoords, angle, false);
        tank.transform.up = newDirection.ToVector();
        tankCoords = new Coords(tank.transform.up);

       energyAmount.text = Convert.ToString(ErgoMath.Distance(tankCoords,fuelCoords));
    }

    // Update is called once per frame
    void Update()
    {

    }
}
