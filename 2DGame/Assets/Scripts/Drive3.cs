﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drive3 : MonoBehaviour
{
    public GameObject fuel;

    float speed = 5.0f;
    float stoppingDistance = 0.1f;
    Vector3 direction;

    void Start()
    {
        direction = fuel.transform.position - this.transform.position;
        Coords dirNormal = ErgoMath.GetNormal(new Coords(direction));
        direction = dirNormal.ToVector();

        transform.up = ErgoMath.LookAt2D(new Coords(this.transform.up),
                                        new Coords(this.transform.position),
                                        new Coords(fuel.transform.position)).ToVector();
    }

    // Update is called once per frame
    void Update()
    {
        if (ErgoMath.Distance(new Coords(this.transform.position), new Coords(fuel.transform.position)) > stoppingDistance)
            this.transform.position += direction * speed * Time.deltaTime;
    }
}
