﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneRayIntersection : MonoBehaviour
{
    public GameObject quad;
    public GameObject sphere;
    public Transform corner1;
    public Transform corner2;
    public Transform corner3;

    Plane mPlane;

    // Start is called before the first frame update
    void Start()
    {
        Vector3[] vectices = quad.GetComponent<MeshFilter>().mesh.vertices;
        mPlane = new Plane(quad.transform.TransformPoint(vectices[0]), 
                            quad.transform.TransformPoint(vectices[1]), 
                            quad.transform.TransformPoint(vectices[2]));
        Debug.Log(vectices[0] + ", " + vectices[1] + ", " + vectices[2]);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButton(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            float t = 0.0f;

            if (mPlane.Raycast(ray, out t))
            {
                Vector3 hitPoint = ray.GetPoint(t);
                sphere.transform.position = hitPoint;
            }
        }
    }
}
